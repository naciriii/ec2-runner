const assert = require('chai').assert
const GeneralMath = require('../src/GeneralMath')
describe('GeneralMath class Testsuites', () => {
  let generalMath
  before(() => {
    generalMath = new GeneralMath()
  })
  it('Return Number', async function () {
    assert.isNumber(generalMath.sum(5, 4))
  })

  it('Throws Error on empty arguments', () => {
    assert.throws(generalMath.sum, Error, 'Arguments Not Found')
  })

  it('Throws Error if arguments not la numbers', () => {
    assert.throws(() => { generalMath.sum(1, 2, 'B') }, Error, 'Elements Must Be Numbers')
  })

  it('Returns the correct Result', () => {
    assert.strictEqual(generalMath.sum(5, 4, 1), 10)
  })

  after(async () => {
  })
})
