module.exports = class GeneralMath {
  sum (...args) {
    if (!args.length) {
      throw Error('Arguments Not Found')
    }
    if (!args.every(el => !isNaN(el))) {
      throw Error('Elements Must Be Numbers')
    }
    const result = args.reduce((prev, acc) => {
      return prev + acc
    })
    return result
  }
}
